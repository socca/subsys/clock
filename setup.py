#!/usr/bin/env python
# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2018 BayLibre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from setuptools import setup

setup(
    name='SoccaClock',
    packages=['soccaclock'],
    author='Alexandre Bailon',
    author_email='abailon@baylibre.com',
    description='Provide some functions to manage plugins and packages.',
    url='https://github.com/BayLibre/SoccaClock',
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "License :: MIT",
        "Natural Language :: English",
        "Operating System :: GNU/Linux",
        "Programming Language :: Python :: 3.6",
    ],
    install_requires=['LibSocca'],
    dependency_links=[
        'git+https://gitlab.com/socca/lib/libsocca.git#egg=LibSocca',
    ],
    entry_points={
        'socca': [
                'init_args = soccaclock.plugin:init_args',
                'process_args = soccaclock.plugin:process_args',
        ]
    },
)

setup(
    name='SoccaClockTest',
    packages=['soccaclocktest'],
    author='Alexandre Bailon',
    author_email='abailon@baylibre.com',
    description='Tests for SoccaClock',
    url='https://github.com/BayLibre/SoccaClock', 
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "License :: MIT",
        "Natural Language :: English",
        "Operating System :: GNU/Linux",
        "Programming Language :: Python :: 3.6",
    ],
    install_requires=['LibSocca', 'SoccaTest'],
    dependency_links=[
        'git+https://gitlab.com/socca/lib/libsocca.git#egg=LibSocca',
        'git+https://gitlab.com/socca/arch/test.git#egg=SoccaTest',
    ],
    entry_points={
        'socca': [
                'run_tests = soccaclocktest.test:run_tests',
        ]
    },
)
